import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
// import { LottieSplashScreen } from '@ionic-native/lottie-splash-screen';
import { StatusBar } from '@ionic-native/status-bar/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  public appPages = [
    {
      title: 'Home',
      url: '/home',
      icon: 'home'
    },
    {
      title: 'List',
      url: '/list',
      icon: 'list'
    },
    {
      title: 'Starred',
      url: '/starred',
      icon: 'star'
    }
  ];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    // private lottieSpashScreen: LottieSplashScreen
  ) {
    this.initializeApp();
  }

  initializeApp() {
    // this.lottieSpashScreen.show('www/lottie/animation.json', false, 1024, 768);
    // .then((res: any) => console.log(res))
    // .catch((error: any) => console.error(error));

    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      // setTimeout(() => {
      //   this.lottieSpashScreen.hide()
      // }, 4000)
      this.splashScreen.hide();
    });
    // Use matchMedia to check the user preference
    const prefersDark = window.matchMedia('(prefers-color-scheme: dark)');

    this.toggleDarkTheme(prefersDark.matches);

    // Listen for changes to the prefers-color-scheme media query
    // prefersDark.addListener((mediaQuery) => this.toggleDarkTheme(mediaQuery.matches));
  }

  // Add or remove the "dark" class based on if the media query matches
  toggleDarkTheme(shouldAdd) {
    document.body.classList.toggle('dark', shouldAdd);
  }
}
