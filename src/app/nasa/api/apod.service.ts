import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { NasaApod } from '../model/apod'

@Injectable({
  providedIn: 'root'
})
export class ApodService {


  private apiKey: string = 'FuWrKJUaZ8ADfr4iiIIUPMCArzpBv2XoQHAwSd1P';
  private nasaRoot = 'https://api.nasa.gov/planetary/apod';

  constructor(private http: HttpClient) {

  }

  /* GET heroes whose name contains search term */
  getPictureOfDay(): Observable<NasaApod> {
    // term = term.trim();

    // // Add safe, URL encoded search parameter if there is a search term
    // const options = term ?
    //   { params: new HttpParams().set('api_key', this.apiKey) } : {};

    const options = { params: new HttpParams().set('api_key', this.apiKey) };

    return this.http.get<NasaApod>(this.nasaRoot, options);
    //   .subscribe(
    //     data => {
    //       console.log("PUT Request is successful ", data);
    //     },
    //     error => {
    //       console.log("Rrror", error);
    //     }
    //   );
  }

  getImage(nasaApod: NasaApod): Observable<Blob> {
    return this.getImageFromUrl(nasaApod.url);
  }

  getImageHd(nasaApod: NasaApod): Observable<Blob> {
    return this.getImageFromUrl(nasaApod.hdurl);
  }

  private getImageFromUrl(url: string): Observable<Blob> {
    const options = { params: new HttpParams().set('api_key', this.apiKey) , responseType: "blob"};
    const options2 = { responseType: "blob" };
    return this.http.get(url, { responseType: "blob" });
    //return this.http.get(url, { params: new HttpParams().set('api_key', this.apiKey) , responseType: "blob"});
  }
}

