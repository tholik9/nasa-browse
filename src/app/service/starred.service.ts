import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { VirtualTimeScheduler } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StarredService {

  constructor(public storage: Storage) { }

  public addStarredItem(id: string, name: string) {
    this.storage.set(id, name)
      .then(data => {
        console.info("Item saved successfully.")
      }, error => {
        console.error("Error during save of item.")
      });
  }

  public async getStarredData() {
    let items = [];
    await this.storage.forEach((name, id) => {
      ; items.push({ id, name })
    });
    return items;
  }

  public getById(id: string) {
    return this.storage.get(id)
      .then(data => {
        console.info("Item saved successfully.")
        return data;
      }, error => {
        console.error("Error during save of item.")
      });
  }

  public removeById(id: string) {
    return this.storage.remove(id)
      .then(data => {
        console.info("Item removed successfully.")
        return data;
      }, error => {
        console.error("Error during remove of item.")
      });
  }
}
