import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'
import { FeedService, SentryObjectPagingDto, NearEarthObjectList } from '../../nasa';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions/ngx';
import { Platform, NavController } from '@ionic/angular';
import { ToastController } from '@ionic/angular';
import { LoadingController } from '@ionic/angular';
import { StarredService } from '../../service/starred.service'

@Component({
  selector: 'app-list',
  templateUrl: 'list.page.html',
  styleUrls: ['list.page.scss']
})
export class ListPage implements OnInit {
  private selectedItem: any;
  protected dateFormat = 'dd/MM/yyyy';
  protected dateFrom: string = new Date().toISOString();//formatDate(new Date().toISOString(), this.dateFormat, 'en-US');
  protected dateTo: string = this.addDays(new Date, 7).toISOString();//formatDate(new Date().toISOString(), this.dateFormat, 'en-US');
  protected sentryObjectPagingDto: SentryObjectPagingDto = { sentry_objects: [] };
  protected nearEarthObjectList: NearEarthObjectList = {};
  public platform: any;

  public items: Array<{ title: string; note: string; icon: string }> = [];

  constructor(private feedService: FeedService,
    private _platform: Platform,
    private nativePageTransitions: NativePageTransitions,
    private router: Router,
    public toastController: ToastController,
    public loadingController: LoadingController,
    public starredService: StarredService) {
    this.platform = _platform;
  }

  ngOnInit() {
    this.browseNearEarthObjectList();
  }

  public loadAsteroidDetail(sentryId: String) {
    // let options: NativeTransitionOptions = {
    //   direction: 'left',
    //   duration: 500,
    //   slowdownfactor: -1,
    //   iosdelay: 50
    // };
    // this.nativePageTransitions.slide(options);
    this.router.navigate(["/asteroid", sentryId]);
  }

  public async browseNearEarthObjectList() {
    if (this.dateFrom > this.dateTo) {
      this.showDateError('Date from must be greater than date to.');
      return;
    }

    const loading = await this.loadingController.create({
      message: 'Please wait...',
      mode: 'ios',
    });
    await loading.present();

    this.feedService.retrieveNearEarthObjectFeed(this.dateFrom, this.dateTo, false)
      .subscribe(data => {
        this.nearEarthObjectList = data;
      }, error => {
        this.showDateError(error);
        console.error("Sentry objects could not be loaded.")
      });

    await loading.dismiss();
  }

  addDays(date: Date, days: number): Date {
    date.setDate(date.getDate() + days);
    return date;
  }

  async starItem(itemId: string, name: string) {
    this.starredService.addStarredItem(itemId, name);
    const toast = await this.toastController.create({
      message: 'Starred successfully',
      position: 'bottom',
      color: 'success',
      duration: 1000,
      showCloseButton: true,
    });
    toast.present();

  }

  async showDateError(msg: string) {
    const toast = await this.toastController.create({
      header: 'Invalid date',
      message: msg,
      position: 'top',
      color: 'danger',
      duration: 4000,
      showCloseButton: true,
    });
    toast.present();
  }
  // showDatepicker(){
  //   this.datePicker.show({
  //     date: new Date(),
  //     mode: 'date',
  //     androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK,
  //     okText:"Save Date",
  //     todayText:"Set Today"
  //   }).then(
  //     date => {
  //       this.myDate = date.getDate()+"/"+date.toLocaleString('default', { month: 'long' })+"/"+date.getFullYear();
  //     },
  //     err => console.log('Error occurred while getting date: ', err)
  //   );
  // }  

  // add back when alpha.4 is out
  // navigate(item) {
  //   this.router.navigate(['/list', JSON.stringify(item)]);
  // }
}
