import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { switchMap, map } from "rxjs/operators" // RxJS v6
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions/ngx';
import { Platform, NavController } from '@ionic/angular';
import {NeoService, NearEarthObject} from '../../nasa'
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-asteroid',
  templateUrl: './asteroid.page.html',
  styleUrls: ['./asteroid.page.scss'],
})
export class AsteroidPage implements OnInit {

  nearEarthObject: NearEarthObject;

  constructor(private nativePageTransitions: NativePageTransitions,
              private navCtrl: NavController,
              private activatedRoute: ActivatedRoute,
              private neoService: NeoService,
              public loadingController: LoadingController) { }

  ngOnInit() {
    let id = this.activatedRoute.snapshot.paramMap.get('id');
    // let id2 = this.activatedRoute.paramMap.pipe(map(paramMap => paramMap.get("id")));
    this.loadNearEarthObjectById(id);
  }

  async loadNearEarthObjectById(id: string) {
    const loading = await this.loadingController.create({
      message: 'Please wait...',
      mode: 'ios',
    });
    await loading.present();

    this.neoService.retrieveNearEarthObjectById(id)
    .subscribe((data: NearEarthObject) => {  
      this.nearEarthObject = data;
    }, (error: any) => {
        console.error("Near Earth object could not be loaded.")
    });

    await loading.dismiss();
  }

  goBack() {
    let options: NativeTransitionOptions = {
      direction: 'down',
      duration: 500,
      slowdownfactor: -1,
      // iosdelay: 50
      slidePixels: 20
    };
    this.nativePageTransitions.slide(options);
    // if
    this.navCtrl.back();
    // this.navCtrl.navigateRoot('home');
  }
}
