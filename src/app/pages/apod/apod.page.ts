import { Component, OnInit } from '@angular/core';
import { ApodService, NasaApod } from '../../nasa';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-apod',
  templateUrl: './apod.page.html',
  styleUrls: ['./apod.page.scss'],
})
export class ApodPage implements OnInit {

  private isImageLoading: boolean;
  imageToShow: any;

  protected nasaApodData: NasaApod = {
    date: null,
    explanation: null,
    hdurl: null,
    media_type: null,
    service_version: null,
    title: null,
    url: null,
  };

  constructor(private apodService: ApodService,
              public loadingController: LoadingController) { }

  ngOnInit() {
    this.isImageLoading = false;
    this.loadApod();
  }

  async loadApod() {
    const loading = await this.loadingController.create({
      message: 'Please wait...',
      mode: 'ios',
    });
    await loading.present();

    this.apodService.getPictureOfDay()
      .subscribe(
        data => {
          this.nasaApodData = data;
          //this.loadImageFromNasaApod(this.nasaApodData);
          console.log("Get Request is successful ", data);
        },
        error => {
          console.log("Error", error);
        }
      );
    await loading.dismiss();
  }

  private loadImageFromNasaApod(nasaApod: NasaApod) {
    this.apodService.getImage(this.nasaApodData)
      .subscribe(data => {
        this.createImageFromBlob(data);
        this.isImageLoading = false;
      }, error => {
        this.isImageLoading = false;
        console.log(error);
      });
  }

  private createImageFromBlob(image: Blob) {
    let reader = new FileReader();
    reader.addEventListener("load", () => {
      this.imageToShow = reader.result;
    }, false);
    if (image) {
      reader.readAsDataURL(image);
    }
  }

}
