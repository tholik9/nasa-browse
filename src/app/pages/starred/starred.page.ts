import { Component, OnInit } from '@angular/core';
import { StarredService } from '../../service/starred.service'
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions/ngx';
import { Router } from '@angular/router'
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-starred',
  templateUrl: './starred.page.html',
  styleUrls: ['./starred.page.scss'],
})
export class StarredPage implements OnInit {

  public starredData;

  constructor(public starredService: StarredService,
    private nativePageTransitions: NativePageTransitions,
    private router: Router,
    public toastController: ToastController,) { }

  ngOnInit() {
    this.refreshStarredData();
  }

  async refreshStarredData() {
    let result = await this.starredService.getStarredData();
    this.starredData = result;
  }

  async removeStarredItem(id: string) {
    await this.starredService.removeById(id);

    const toast = await this.toastController.create({
      message: 'Remove from starred successfully',
      position: 'bottom',
      color: 'success',
      duration: 1000,
      showCloseButton: true,
    });
    toast.present();

    this.refreshStarredData();
  }

  public loadAsteroidDetail(sentryId: String) {
    let options: NativeTransitionOptions = {
      direction: 'left',
      duration: 500,
      slowdownfactor: -1,
      iosdelay: 50
    };
    this.nativePageTransitions.slide(options);
    this.router.navigate(["/asteroid", sentryId]);
  }
}