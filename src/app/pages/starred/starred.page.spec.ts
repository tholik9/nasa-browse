import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StarredPage } from './starred.page';

describe('StarredPage', () => {
  let component: StarredPage;
  let fixture: ComponentFixture<StarredPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StarredPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StarredPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
