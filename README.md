# NASA browse

Ionic application which show details about near earth objects (asteroid) and on main page is astrological picture of day.

Using open REST NASA API from: https://api.nasa.gov
- APOD - Astrological Picture Of Day
- NeoWs - (Near Earth Object Web Service) 
    - swagger API
    - https://api.nasa.gov/neo/?api_key=DEMO_KEY

Swagger yaml downloaded from:
https://github.com/APIs-guru/openapi-directory/blob/master/APIs/neowsapp.com/1.0/swagger.yaml

-ionic cordova build ios
-ionic cordova emulate ios

Ionic 5.0.0
Angular
Apache Cordova